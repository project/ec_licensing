<?php

/**
 * @file
 * Contains administrative interface
 */

/**
 * Admin settings form
 *
 * @todo: add settings for qty of keys per purchase, 
 */
function ec_licensing_form_config() {
  $workflow_status = ec_store_transaction_workflow('types');
  foreach ($workflow_status as $id => $status) {
    $options[$id] = ucfirst($status['description']);
  }

  $form = array(
    'ec_licensing_workflow' => array(
      '#type' => 'select',
      '#title' => t('Transaction workflow'),
      '#description' => t('Select the status a transaction should reach when the license keys are distributed'),
      '#options' => $options,
      '#default_value' => variable_get('ec_licensing_workflow', EC_LICENSING_WORKFLOW_DEFAULT),
    ),
  );
  
  $form['mail'] = array(
    '#type' => 'fieldset',
    '#title' => t('Notification'),
    '#description' => t('Configuration for the notification email'),
    'ec_licensing_mail_header' => array(
      '#type' => 'textarea',
      '#title' => t('Header'),
      '#description' => t('Text at the top of the email.'),
      '#rows' => 4,
      '#default_value' => variable_get('ec_licensing_mail_header', ec_licensing_mail_default('header')),
    ),
    'ec_licensing_mail_product' => array(
      '#type' => 'textarea',
      '#title' => t('Product'),
      '#description' => t('Text for each product. Note, [license-key] is repeated for as many keys as necessary. <small>Disabled temporarily until a better way to do this arises.</small>'),
      '#rows' => 3,
      '#default_value' => variable_get('ec_licensing_mail_product', ec_licensing_mail_default('product')),
      '#disabled' => TRUE,
    ),
    'ec_licensing_mail_footer' => array(
      '#type' => 'textarea',
      '#title' => t('Footer'),
      '#description' => t('Text at the end of the email.'),
      '#rows' => 8,
      '#default_value' => variable_get('ec_licensing_mail_footer', ec_licensing_mail_default('footer')),
    ),
  );
  
  return system_settings_form($form);
}
 
 