
<---! LICENSING ECOMMERCE !--->

This module allows associating certain ecommerce products with the Licensing module.

##################
## DEPENDENCIES ##
##################
This module depends on
 - Licensing (http://drupal.org/project/licensing)
 - Ecommerce 4 (http://drupal.org/project/ecommerce)
 
####################
## BASIC WORKFLOW ##
####################
1. When creating the product, you mark it as 'licensed' in the product settings 
   fieldset.

2. Configure the workflow status you want the transaction to reach before 
   sending the email with the license keys

3. When your customers purchase that product, once it reaches the predetermined 
   workflow status it will send an email with all of the license keys for each 
   product, and further instructions.
   
########################
## SETUP INSTRUCTIONS ##
########################
1. Enable the Licensing Ecommerce module

2. Go to yoursite.com/admin/user/permissions and grant the roles provided by 
   this module to the appropriate roles.
   
3. Head to yoursite.com/admin/settings/licensing/ecommerce and select the 
   status a transaction must reach to send a notification of the license
   keys (I recommend 'Completed' by default)
   
4. Any products that you wish to distribute license keys with, on the 
   node edit form, select 'Licensed' from the product fieldset.
   
5. Now it should be sending emails when the licensed products hit 'completed',
   or whatever you configured it to be.